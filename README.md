Thông tin chi tiết Nồi chiên không dầu Tefal FW201815:

Tham khảo: https://hiyams.com/noi-dien/noi-chien-khong-dau/noi-chien-khong-dau-tefal/noi-chien-khong-dau-tefal-fw201815/

Nồi chiên không dầu Tefal FW201815 đa năng có thể chiên, nướng, hấp thực phẩm mà không cần dầu hoặc ít dầu. Đa dạng các món ăn gia đình với 9 chương trình được cài đặt sẵn.

Nồi chiên không dầu Tefal FW201815 dễ sử dụng và thao tác nhờ bảng điều khiển cảm ứng. Chế biến những món chiên, nướng lành mạnh chưa bao giờ dễ dàng cùng nồi chiên không dầu Tefal.

Nồi chiên không dầu Tefal FW201815 3 trong 1, có thể chiên, nướng và hấp thực phẩm mà không cần dầu hoặc ít dầu.

Thông số kỹ thuật Nồi chiên không dầu Tefal FW201815:

Công nghệ: Khí nóng đa chiều, không cần đảo khi nấu

Bảng điều khiển: Cảm ứng, đèn sáng mỗi chức năng, dễ sử dụng và thao tác

Chức năng: Chiên, nướng và hấp thực phẩm không cần dầu mỡ hoặc ít dầu.

Chương trình nấu: 9

Hẹn giờ nấu: 60 phút. Tự động ngắt điện, chuông báo khi thức ăn đã chín

Dung tích: 4.2 L - 1.2 kg

Dung tích nồi hấp: 1.1 L nước, hấp trong 70-85 phút

Màu sắc: Đen

Xuất xứ: Trung Quốc

Kích thước: 414 x 414 x 435 mm (DxRxC)

Khối lượng: 7.9 kg

Bảo hành: 2 năm
